## Задание 1

Создать приложение, которое принимает в качестве аргумента строку и определяет количество слов в ней.

---
## Задание 2

Составить список учебной группы, включающий Х человек. Для каждого студента указать: фамилию и имя, дату рождения (год, месяц и число), также у каждого студента есть зачетка, в которой указаны: предмет (от трех до пяти), дата экзамена, ФИО преподавателя.

Вывести ФИО и дату рождения, самого младшего и самого старшего студента.

---
## Дополнительное задание

Чтобы при деплое в разные ветки запускались разные стадии.
При коммите в ветку dev запускалась стадия deploy-dev, а в master - deploy-master. При этом гитлаб файл должен быть один.