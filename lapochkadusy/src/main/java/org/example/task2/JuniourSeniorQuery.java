package org.example.task2;

import java.sql.*;

public class JuniourSeniorQuery {
    private static String JDBC_URL = "jdbc:postgresql://postgres:5432/";

    public void jSQuery() throws SQLException {

        Connection connection = null;

        try {
            JDBC_URL = JDBC_URL.concat(System.getenv("POSTGRES_DB"));
            connection = DriverManager.getConnection(JDBC_URL, System.getenv("POSTGRES_USER") ,System.getenv("POSTGRES_PASSWORD"));
            String youngestQuery = "SELECT nameStudent, surnameStudent, birthdateStudent FROM students ORDER BY birthdateStudent DESC LIMIT 1";
            printStudentInfo(connection, youngestQuery, "Самый маленький студент:");

            String oldestQuery = "SELECT nameStudent, surnameStudent, birthdateStudent FROM students ORDER BY birthdateStudent ASC LIMIT 1";
            printStudentInfo(connection, oldestQuery, "Самый старший студент:");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            assert connection != null;
            connection.close();
        }
    }

    private void printStudentInfo(Connection connection, String sqlQuery, String message) throws SQLException {
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
                ResultSet resultSet = preparedStatement.executeQuery()
            ) {
            if (resultSet.next()) {
                String name = resultSet.getString("nameStudent");
                String surname = resultSet.getString("surnameStudent");
                String birthDate = resultSet.getString("birthdateStudent");
                System.out.println(message);
                System.out.println("Имечко: " + name + " " + surname);
                System.out.println("Дата рождения: " + birthDate);
                System.out.println();
            }
        }
    }
}
