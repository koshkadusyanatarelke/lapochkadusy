package org.example.task1;

import java.util.StringTokenizer;

public class CountingWords {

    public void countWords(){
        String inputString = System.getenv("SLOVA_DUSY");

        StringTokenizer sk = new StringTokenizer(inputString, " ,.?!;");

        if (sk.countTokens() > 0) {
            System.out.println("Количество слов: " + sk.countTokens());
        } else {
            System.out.println("Нет слов");
        }
    }
}
