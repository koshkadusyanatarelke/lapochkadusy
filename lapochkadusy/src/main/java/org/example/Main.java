package org.example;

import org.example.task1.CountingWords;
import org.example.task2.JuniourSeniorQuery;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException {
        String task = System.getenv("TASK_DUSY");

        switch (task) {
            case "task1" -> {
                CountingWords cw = new CountingWords();
                cw.countWords();
            }
            case "task2" -> {
                JuniourSeniorQuery jsq = new JuniourSeniorQuery();
                jsq.jSQuery();
                ;
            }
            default -> System.out.println("No task");
        }
    }
}